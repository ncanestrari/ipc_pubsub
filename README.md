# IPC Publisher Subscriber

IPC\_PubSub provides a simple Inter Process Communication platform via mqueues, where several process can interchange messages using a topic-based subscription
the framework limits the maximum number of process to 32. Other limits are linked to the underline mqueue configuration:

```
ulimit -q
```

generally this is set to 819200 bytes
The infrastructure dedicates one mqueue to each process, each mqueue is imlemented to have msg_max 8 and msgsize_max 128.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

In order to run the test the following software is required:
 - kernel >= 2.6.6
 - libpthread
 - librt

check via

```
uname -r
locate libpthread.so
locate librt.so
```

which are aailable under all modern linux distribution

In order to compile:
 - cmake >= 3.1.0
 - make
 - gcc

install under debian via:

```
aptitude install cmake make gcc
```

In order to compile the documentation:
 - doxygen
 - dot

```
aptitude install doxygen graphviz
```

 
### Installing

Compile Debug version via

```
mkdir Debug/
cd Debug/
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

Compile Release version via

```
mkdir Release/
cd Release/
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```


## Running the tests

A test with four processes (process{0..3}) is available in the binary folder
they can be run in parallel using the bash script ./runner 

```
./runner
```

logs about sent and received messages are written in the files process{0..3}.log

## Miscellaneous

![image Info](info/schematics.jpg)
