/**
 * @file
 */

#pragma once

#define NB_PROCESS_MAX 32

#include "topic.h"
#include <mqueue.h>
#include <stddef.h>

#define CONTENT_SIZE 120

/**
 * @brief       message generic structure
 * @details     passing a message via inter process communication imposes a
 * limit on the message size hence a lack of generality from a high level
 * prospective
 */
typedef struct {
  char content[CONTENT_SIZE];
  int topic;
  unsigned size;
} message_t;

#define MSG_SIZE sizeof(message_t)

/**
 * @brief           Initializes the singleton (per process) broadcaster (master
 * process only)
 * @details         the broadcaster shared memory is set up and initialized zero
 *                  the broadcaster mqueues are set up
 *                  the broadcaster semaphore is initialized and set to one
 * (unlocked)
 * @param[in]       nb_proc - number of processes to connect (max 32)
 */
void request_broadcaster(int nb_proc);

/**
 * @brief           Initializes the singleton (per process) broadcaster (slave
 * processes only)
 * @details         the broadcaster shared memory is obtained and connected
 *                  the broadcaster mqueues are obtained and connected
 *                  the broadcaster semaphore is obtained and connected
 * @param[in]       nb_proc - number of processes to connect (max 32)
 */
void request_broadcaster_client(int nb_proc);

/**
 * @brief           Releases the shared resources initialize by
 *                  request_broadcaster
 * @details         the broadcaster shared memory is unlinked
 *                  the broadcaster mqueues are unlinked
 *                  the broadcaster semaphore is unlinked
 */
void release_broadcaster();

/**
 * @brief           Releases the shared resources initialize by
 *                  request_broadcaster_client
 * @details         the broadcaster shared memory is closed
 *                  the broadcaster mqueues are closed
 *                  the broadcaster semaphore is closed
 */
void release_broadcaster_client();

/**
 * @brief           Subscribes a given process to a given topic
 * @details         if topic or proc are not valid it skips
 *                  concurrency ensured by semaphore locking mechanism
 * @param[in]       proc - {0..nb_proc} running process ID (not PID)
 * @param[in]       topic - see e_TOPIC
 */
void subscribe(int proc, int topic);

/**
 * @brief           Publishes the new message to the mqueue of the concerned
 * processes
 * @param[in]       topic - see e_TOPIC
 * @param[in]       what - content to send
 * @param[in]       size - size of content
 */
void notify(int topic, const char *what, size_t size);

/**
 * @brief           Fills a message with a given content
 * @param[in]       topic - see e_TOPIC
 * @param[in]       what - content to fill
 * @param[in]       size - size of the content
 * @param[out]      out - message to be filled
 */
void make_message(int topic, const char *what, size_t size, message_t *out);

/**
 * @brief           Sends a given message to a given process using the
 *                  infrastructure
 * @param[in]       proc - {0..nb_proc} running process ID (not PID)
 * @param[in]       mess - message to deliver
 */
void send_message(int proc, message_t *mess);

/**
 * @brief           Receives a given message for a given process using the
 * infrastructure
 * @param[in]       proc - {0..nb_proc} running process ID (not PID)
 * @param[out]      mess - message to fetch
 */
void recv_message(int proc, message_t *mess);
