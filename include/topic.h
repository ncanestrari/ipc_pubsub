/**
 * @file
 */

#pragma once

/**
 * @brief           topic enumerator has to be extended manually
 */
typedef enum {
  TOPIC000,
  TOPIC001,
  TOPIC002,
  TOPIC003,
  TOPIC004,
  TOPIC005,
  TOPIC006,
  TOPIC007,
  TOPIC008,
  TOPIC009,
  TOPIC010,
  TOPIC011,
  TOPIC_NB,
  TOPIC_QUIT = TOPIC011,
} e_TOPIC;
