/**
 * @file
 */

#include "broadcaster.h"

#include <errno.h>
#include <fcntl.h>
#include <mqueue.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>


#include <stdio.h>
#include <string.h>

#define SHARED_MEM_NAME "/ipc-ant"
#define MQUEUE_NAME "/ant-proc-%d"
#define SEMAPHORE_NAME "/sem-ant"
#define MQNAME_SIZE 32
#define PRIO 1
#define MAX_MSG 8
#define PERMISSION 0660
#define DELEGATE 0
#define BUFFER_SIZE 256

typedef int key_t;

typedef struct {
  key_t *subscription;
  mqd_t mqueues[NB_PROCESS_MAX];
  int nb_proc;
  sem_t *sem;
} shm_bc;

static int has_subscribed(int proc, int topic);
static int check_bound(int proc, int topic);

static shm_bc bc;

void request_broadcaster(int nb_proc) {
  bc.sem = sem_open(SEMAPHORE_NAME, O_CREAT, PERMISSION, 1);
  
  sem_wait(bc.sem);
  int fd_shm = shm_open(SHARED_MEM_NAME, O_RDWR | O_CREAT, PERMISSION);
  if (fd_shm == -1)
    perror("request_broadcaster shm_open");

  int ft = ftruncate(fd_shm, sizeof(int) * TOPIC_NB);
  if (ft == -1)
    perror("request_broadcaster: ftruncate");

  bc.subscription = mmap(NULL, sizeof(int) * TOPIC_NB, PROT_READ | PROT_WRITE,
                         MAP_SHARED, fd_shm, 0);
  if (bc.subscription == MAP_FAILED)
    perror("request_broadcaster: mmap");

  // initialize the shared memory
  for (int i = 0; i < TOPIC_NB; ++i)
    bc.subscription[i] = 0;

  bc.nb_proc = (nb_proc < 1) ? NB_PROCESS_MAX : nb_proc;

  for (int i = 0; i < bc.nb_proc; ++i) {
    char buffer[MQNAME_SIZE];
    snprintf(buffer, MQNAME_SIZE, MQUEUE_NAME, i);
    struct mq_attr attr;
    attr.mq_flags = 0;
    attr.mq_maxmsg = MAX_MSG;
    attr.mq_msgsize = MSG_SIZE;
    attr.mq_curmsgs = 0;
    bc.mqueues[i] = mq_open(buffer, O_RDWR | O_CREAT, PERMISSION, &attr);
    if (bc.mqueues[i] == (mqd_t)-1)
      perror("request_broadcaster: mq_open");
  }
  sem_post(bc.sem);
}

void request_broadcaster_client(int nb_proc) {
  bc.sem = sem_open(SEMAPHORE_NAME, DELEGATE);
  sem_wait(bc.sem);
  int fd_shm = shm_open(SHARED_MEM_NAME, O_RDWR, DELEGATE);
  if (fd_shm == -1)
    perror("request_broadcaster_client: shm_open");

  bc.subscription = mmap(NULL, sizeof(int) * TOPIC_NB, PROT_READ | PROT_WRITE,
                         MAP_SHARED, fd_shm, 0);
  if (bc.subscription == MAP_FAILED)
    perror("request_broadcaster_client: mmap");

  bc.nb_proc = (nb_proc < 1) ? NB_PROCESS_MAX : nb_proc;

  for (int i = 0; i < bc.nb_proc; ++i) {
    char buffer[MQNAME_SIZE];
    snprintf(buffer, MQNAME_SIZE, MQUEUE_NAME, i);
    bc.mqueues[i] = mq_open(buffer, O_RDWR);
    if (bc.mqueues[i] == (mqd_t)-1)
      perror("request_broadcaster: mq_open");
  }
  sem_post(bc.sem);
}

void release_broadcaster() {
  for (int i = 0; i < NB_PROCESS_MAX; ++i) {
    char buffer[MQNAME_SIZE];
    snprintf(buffer, NB_PROCESS_MAX, MQUEUE_NAME, i);
    mq_unlink(buffer);
    mq_close(bc.mqueues[i]);
  }
  shm_unlink(SHARED_MEM_NAME);
  sem_unlink(SEMAPHORE_NAME);
}

void release_broadcaster_client() {
  for (int i = 0; i < TOPIC_NB; ++i) {
    mq_close(bc.mqueues[i]);
  }
  munmap(bc.subscription, sizeof(int) * TOPIC_NB);
  sem_close(bc.sem);
}

void subscribe(int proc, int topic) {
  if (check_bound(proc, topic)) {
    sem_wait(bc.sem);
    bc.subscription[topic] |= 1 << proc;
    sem_post(bc.sem);
  }
}

void notify(int topic, const char *what, size_t size) {
  for (int i = 0; i < bc.nb_proc; ++i)
    if (has_subscribed(i, topic)) {
      message_t mess;
      make_message(topic, what, size, &mess);
      send_message(i, &mess);
    }
}

void make_message(int topic, const char *what, size_t size, message_t *out) {
  out->topic = topic;
  out->size = (size > CONTENT_SIZE) ? CONTENT_SIZE : size;
  memcpy(out->content, what, out->size);
}

void send_message(int proc, message_t *mess) {
  const char * ptr = (const char *) mess;
  sem_wait(bc.sem);
  if (mq_send(bc.mqueues[proc], ptr, MSG_SIZE, PRIO) == -1) {
    char buffer[BUFFER_SIZE];
    sprintf(buffer, "send_message: mq_send (proc %d - mq %d - topic %d - mess - %s)",
            proc, bc.mqueues[proc], mess->topic, mess->content);
    perror(buffer);
  }
#ifdef DEBUG
  else
    printf("Message Sent: %02d - %02d - %s\n", proc, mess->topic, mess->content);
#endif /* DEBUG */
  sem_post(bc.sem);
}

void recv_message(int proc, message_t *mess) {
  if (mq_receive(bc.mqueues[proc], (char *)mess, MSG_SIZE, NULL) == -1)
    perror("recv_message: mq_receive");
#ifdef DEBUG
  else
    printf("Message Recv: %02d - %02d - %s\n", proc, mess->topic, mess->content);
#endif /* DEBUG */

  if (!check_bound(proc, mess->topic))
    perror("recv message: non conform message received");
}

static int has_subscribed(int proc, int topic) {
  int ret = 0;
  if (check_bound(proc, topic))
    ret = bc.subscription[topic] & (1 << proc);
  return ret;
}

static int check_bound(int proc, int topic) {
  return (proc >= 0 && proc < bc.nb_proc && 
          topic >= TOPIC000 && topic < TOPIC_NB);
}
