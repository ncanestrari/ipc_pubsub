
/**
 * @file
 */

#include "process.h"
#include "broadcaster.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static int greater(const void * l, const void * r);

void *sender(void *arg) {
  sender_arg * in = (sender_arg *) arg;  
  const char *mess = "From PROC ";
  char buffer[32];
  sprintf(buffer, "%s%d", mess, in->proc);
  for (int i = 0; i < in->arr_size; ++i) {
    usleep(20000);
    notify(in->arr[i], buffer, strlen(buffer));
  }
  return NULL;
}

void *receiver(void *arg) {
  receiver_arg * in = (receiver_arg *) arg;
  int counter = 0;
  while (1) {
    message_t mess;
    recv_message(in->proc, &mess);
    int proc;
    if (counter < in->ans_size) {
      sscanf(mess.content, "From PROC %d", &proc);
      in->ans[counter] = proc << 16 | mess.topic;
    }
    ++counter;
    if (mess.topic == TOPIC_QUIT) {
      usleep(10000);
      (*in->release)();
      break;
    }
  }
  return NULL;
}

void init(initializer_arg * in) {
  (*in->init)(in->nb_proc);
  for (int  i = 0;i < in->arr_size; ++i) {
    subscribe(in->proc, in->arr[i]);
  }
}

static int greater(const void * l, const void * r) {
  return * (int *) l - * (int *) r;
}

void sort_ans(int * ans, int ans_size) {
  qsort(ans, ans_size, sizeof(int), greater);
}
