/**
 * @file
 */

#pragma once

#define ARRAY_SIZE(arr) sizeof(arr) / sizeof(arr[0])

typedef struct {
  int           proc;
  int           arr_size;
  int          *arr;
} sender_arg;

void *sender(void *arg);

typedef void (* releaser)();
typedef struct {
  int           proc;
  releaser      release;
  int           ans_size;
  int          *ans;
} receiver_arg; 
void *receiver(void *arg);

typedef void (* initializer)(int);
typedef struct {
  int           nb_proc;
  int           proc;
  int           arr_size;
  int          *arr;
  initializer   init;
} initializer_arg;
void init(initializer_arg * in);

void sort_ans(int * ans, int ans_size);
