/**
 * @file
 */

extern "C" {
#include "broadcaster.h"
#include "process.h"

#include <pthread.h>
#include <unistd.h>
}

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "gtest/gtest.h"

#define PROC    1
#define NB_PROC 4

static pthread_t send_id;
static pthread_t recv_id;

static int var[] = {TOPIC000, TOPIC002, TOPIC003};
static sender_arg sarg = {PROC, ARRAY_SIZE(var), var};

#define NB_ANS 8
static int ans[NB_ANS] = {0};
static receiver_arg rarg = {PROC, &release_broadcaster_client, NB_ANS, ans};

static int sub[] = {TOPIC001, TOPIC004, TOPIC005, TOPIC_QUIT};
static initializer_arg iarg = {NB_PROC, PROC, ARRAY_SIZE(sub), sub, &request_broadcaster_client};

static int exp[NB_ANS] = {0x00000001, 0x00000004, 0x00000005, 0x00020004, 0x00030001, 0x00030004, 0x00030005, 0x0003000B};

TEST(process1, NormalCase) {
  usleep(500000);
  init(&iarg);
  usleep(500000);

  pthread_create(&send_id, NULL, sender, &sarg);
  pthread_create(&recv_id, NULL, receiver, &rarg);

  pthread_join(send_id, NULL);
  pthread_join(recv_id, NULL);

  sort_ans(ans, NB_ANS);
  
#ifdef DEBUG
  for (int i = 0; i < NB_ANS; ++i) {
    printf("ans [%d] = %08x exp[%d] = %08x\n", i, ans[i], i, exp[i]);
  }
#endif/* DEBUG */
  for (int i = 0; i < NB_ANS; ++i) {
    ASSERT_EQ (ans[i], exp[i]);
  }
}

